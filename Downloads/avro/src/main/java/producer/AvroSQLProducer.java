package producer;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.sql.*;
import java.util.Properties;


public class AvroSQLProducer {
    static final String TOPIC = "cars-position";
    static final String USER = "root";
    static final String PASSWORD = "memsql";
    static final String DB_CLASS_NAME = "com.mysql.jdbc.Driver";
    static final String CONNECTION = "jdbc:mysql://127.0.0.1:3307/taxi";
    static Properties config;

    public static void main(String[] args) throws ClassNotFoundException {
        KafkaProducer<GenericRecord, GenericRecord> producer;
        ProducerRecord<GenericRecord, GenericRecord> record;
        GenericRecord valueRecord;
        Schema valueSchema;

        initiazlize();

        producer = new KafkaProducer<GenericRecord, GenericRecord>(config);
        valueSchema = SchemaBuilder.record("taxiGPS")
                .fields()
                .name("time").type("int").noDefault()
                .name("car_id").type("string").noDefault()
                .name("group_id").type("int").noDefault()
                .name("latitude").type("double").noDefault()
                .name("longitude").type("double").noDefault()
                .name("status").type("boolean").noDefault()
                .endRecord();


        Class.forName(DB_CLASS_NAME);

        try(Connection con = DriverManager.getConnection(CONNECTION, USER, PASSWORD)) {
            for (int i = 0; i < 1800; i++) {
                String sql = "SELECT time, car_id, group_id, latitude, longitude, status FROM taxi_drive WHERE time = " + i;

                Statement prpstmt = con.createStatement();

                ResultSet rs = prpstmt.executeQuery(sql);


                while (rs.next()) {
                    valueRecord = new GenericData.Record(valueSchema);

                    valueRecord.put("time", rs.getInt("time"));
                    valueRecord.put("car_id", rs.getString("car_id"));
                    valueRecord.put("group_id", rs.getInt("group_id"));
                    valueRecord.put("latitude", rs.getDouble("latitude"));
                    valueRecord.put("longitude", rs.getDouble("longitude"));
                    valueRecord.put("status", Boolean.parseBoolean(rs.getString("status")));


                    record =
                            new ProducerRecord<GenericRecord, GenericRecord>(TOPIC, null, valueRecord);
                    producer.send(record);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    private static void initiazlize() {
        config = new Properties();
        config.put("bootstrap.servers",  "localhost:9092");
        config.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        config.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        config.put("schema.registry.url", "http://localhost:8081");
        config.put("acks",  "all");
    }
}